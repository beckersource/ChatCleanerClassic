ChatCleanerClassic is a customizable chat filter for WoW Classic.

This was created mainly for eliminating unwanted chat in channels like "LookingForGroup".

Use **/ChatCleanerClassic** to open the options panel.

This addon is untested in retail.

Features:
> Option to filter or allow "friendly" players (friends, guildies, etc).

> Option to match keywords as whole words (rather than partial).

> Specify channels by name to be affected by the filtering (example: LookingForGroup).

> Option to filter "say" chats.

> Option to filter "yell" chats.

> Optional "required" keywords which forces a chat to have at least one keyword from the list to be shown.

> Main clean keywords list.

> Allows for exporting and importing of settings between chars on the same account.
